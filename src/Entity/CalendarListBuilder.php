<?php
namespace Drupal\portal_calendar\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class CalendarListBuilder extends ConfigEntityListBuilder {

  public function buildHeader() {
    return [
      'calendar_id' => $this->t('ID'),
      'name' => $this->t('Name'),
      'operations' => $this->t('Operations'),
    ];
  }

  public function buildRow(EntityInterface $entity) {
    $row = array(
      ['data' => $entity->id()],
      ['data' => $entity->toLink()],
      ['data' => $this->buildOperations($entity)],
    );
    return $row;
  }
}
