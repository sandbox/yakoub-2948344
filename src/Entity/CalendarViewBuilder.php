<?php

namespace Drupal\portal_calendar\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;

class CalendarViewBuilder extends EntityViewBuilder {

  public function __construct(EntityTypeInterface $entity_type, EntityManagerInterface $entity_manager, LanguageManagerInterface $language_manager, Registry $theme_registry = NULL) {
    parent::__construct($entity_type, $entity_manager, $language_manager, $theme_registry);
    $this->event_storage = $this->entityManager->getStorage('portal_event');
  }

  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $build = parent::view($entity, $view_mode, $langcode);
    return $build;
  }

  public function buildMultiple(array $build_list) {
    $children = Element::children($build_list);
    $view_modes = [];
    foreach ($children as $key) {
      if (isset($build_list[$key]['#portal_calendar'])) {
        $entity = $build_list[$key]['#portal_calendar'];
        $view_mode = $build_list[$key]['#view_mode'];
        $view_modes[$view_mode][$key] = $entity;
      }
    }
    foreach ($view_modes as $view_mode => $view_mode_entities) {
      $this->buildCalendarComponents($build_list, $view_mode_entities, $view_mode);
    }
    return $build_list;
  }

  public function buildCalendarComponents(array &$build, array $entities, $view_mode) {
    $ids = [];
    foreach ($entities as $calendar) {
      $ids[] = $calendar->id();
    }
    $events = $this->getEvents($ids);
    if ($events) {
      foreach ($entities as $indx => $calendar) {
        $build[$indx]['events'] = $events[$calendar->id()];
      }
    }
  }

  public function getEvents($calendar_ids) {
    $query = $this->event_storage->getQuery();
    $query->condition('calendar_id', $calendar_ids, 'IN');
    $query->range(0, 10);
    //$query->sort('dates');
    $event_ids = $query->execute();
    $calendars = [];
    if ($event_ids) {
      $events = $this->event_storage->loadMultiple($event_ids);
      $builder = $this->entityManager->getViewBuilder('portal_event');
      $calendars_events = [];
      foreach ($events as $event) {
        $calendars_events[$event->bundle()][] = $event;
      }
      foreach ($calendars_events as $calendar_id => $calendar_events) {
        $calendars[$calendar_id] = $builder->viewMultiple($calendar_events);
      }
      return $calendars;
    }
    return NULL;
  }
}
