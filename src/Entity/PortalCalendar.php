<?php

namespace Drupal\portal_calendar\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityTypeInterface;

/**
  * @ConfigEntityType(
  *   id = "portal_calendar",
  *   label = "Calendar",
  *   admin_permission = "administer organization",
  *   entity_keys = {
  *     "id" = "calendar_id",
  *     "label" = "name",
  *     "langcode" = "langcode",
  *   },
  *   bundle_of =  "portal_event",
  *   config_prefix = "calendar",
  *   handlers = {
  *     "list_builder" = "Drupal\portal_calendar\Entity\CalendarListBuilder",
  *     "view_builder" = "Drupal\portal_calendar\Entity\CalendarViewBuilder",
  *     "form" = {
  *       "default" = "Drupal\portal_calendar\Form\CalendarEntityForm",
  *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
  *     },
  *     "route_provider" = {
  *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
  *     }
  *   },
  *   translatable = FALSE,
  *   links = {
  *     "canonical" = "/calendar/{portal_calendar}",
  *     "add-page" = "/calendar/add",
  *     "add-form" = "/calendar/add",
  *     "edit-form" = "/calendar/{portal_calendar}/edit",
  *     "delete-form" = "/calendar/{portal_calendar}/delete",
  *     "collection" = "/admin/portal/calendar",
  *   },
  * )
  */
class PortalCalendar extends ConfigEntityBundleBase {

  protected $calendar_id;

  protected $name;

  protected $langcode;

  public function id() {
    return $this->calendar_id;
  }

  public function label() {
    return $this->name;
  }
}
