<?php

namespace Drupal\portal_calendar\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
  * @ContentEntityType(
  *   id = "portal_event",
  *   label = "Event",
  *   bundle_label = "Calendar",
  *   admin_permission = "administer organization",
  *   base_table = "portal_event",
  *   bundle_entity_type = "portal_calendar",
  *   fieldable = TRUE,
  *   entity_keys = {
  *     "id" = "event_id",
  *     "label" = "name",
  *     "langcode" = "langcode",
  *     "bundle" = "calendar_id",
  *   },
  *   handlers = {
  *     "list_builder" = "Drupal\Core\Config\Entity\EntityListBuilder",
  *     "form" = {
  *       "default" = "Drupal\portal_calendar\Form\EventEntityForm",
  *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
  *     },
  *     "route_provider" = {
  *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
  *     }
  *   },
  *   field_ui_base_route = "entity.portal_calendar.canonical",
  *   common_reference_target = TRUE,
  *   translatable = FALSE,
  *   links = {
  *     "canonical" = "/event/{portal_event}",
  *     "add-page" = "/event/add",
  *     "add-form" = "/event/add/{portal_calendar}",
  *     "edit-form" = "/event/{portal_event}/edit",
  *     "delete-form" = "/event/{portal_event}/delete",
  *   },
  * )
  */
class PortalEvent extends ContentEntityBase {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel('Name')
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['dates'] = BaseFieldDefinition::create('portal_daterange')
      ->setLabel('Dates')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }
}
