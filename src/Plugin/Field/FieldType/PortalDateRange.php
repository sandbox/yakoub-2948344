<?php

namespace Drupal\portal_calendar\Plugin\Field\FieldType;

use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Portal daterange.
 *
 * @FieldType(
 *   id = "portal_daterange",
 *   label = @Translation("Portal Date range"),
 *   description = @Translation("Create and store date ranges."),
 *   default_widget = "daterange_default",
 *   default_formatter = "daterange_default",
 *   list_class = "\Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList"
 * )
 */
class PortalDateRange extends DateRangeItem {
  
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = DateRangeItem::propertyDefinitions($field_definition);
    $properties['start_date']->setClass('\Drupal\portal_calendar\PortalDateComputed');
    $properties['end_date']->setClass('\Drupal\portal_calendar\PortalDateComputed');
    return $properties;
  }

  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = PortalDate::schema($field_definition);

    $schema['columns']['value']['description'] = 'The start date value.';

    $schema['columns']['end_value'] = [
      'description' => 'The end date value.',
    ] + $schema['columns']['value'];

    $schema['indexes']['end_value'] = ['end_value'];

    return $schema;
  }
}
