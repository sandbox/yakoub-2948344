<?php
namespace Drupal\portal_calendar\Plugin\Field\FieldType;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Portal Date.
 *
 * @FieldType(
 *   id = "portal_date",
 *   label = @Translation("Portal Date"),
 *   description = @Translation("Create and store date values."),
 *   default_widget = "datetime_default",
 *   default_formatter = "datetime_default",
 *   list_class = "\Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList",
 *   constraints = {"DateTimeFormat" = {}}
 * )
 */
class PortalDate extends DateTimeItem {
  
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = DateTimeItem::propertyDefinitions($field_definition);
    $properties['date']->setClass('\Drupal\portal_calendar\PortalDateComputed');
    return $properties;
  }

  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'The date value.',
          'type' => 'varchar',
          'length' => 20,
          'mysql_type' => 'datetime',
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }
}
