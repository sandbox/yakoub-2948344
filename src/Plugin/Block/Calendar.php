<?php

namespace Drupal\portal_calendar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Portal calendar.
 *
 * @Block(
 *  id = "portal_calendar",
 *  admin_label = @Translation("Portal Calendar"),
 *  category = @Translation("Portal"),
 * )
 */
class Calendar extends BlockBase implements ContainerFactoryPluginInterface {

  protected $routeMatch;
  protected $entityManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->routeMatch = $route_match;
    $this->entity_manager = $entity_manager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity.manager')
    );
  }

  public function getCacheMaxAge() {
    return 0;
  }

  public function build() {
    return ['#theme' => 'calendar_block'];
  }
}
