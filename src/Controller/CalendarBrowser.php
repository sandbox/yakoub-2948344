<?php

namespace Drupal\portal_calendar\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\portal_calendar\Entity\PortalCalendar;

class CalendarBrowser extends ControllerBase {
  
  public function json_post(PortalCalendar $portal_calendar, Request $request) {
    $post = $request->request;
    if (!$post->has('year') or !$post->has('month')) {
      return new JsonResponse('Missing params', 404);
    }
    $year = $post->get('year');
    $month =  $post->get('month');
    $day = $post->has('day') ? $post->get('day') : NULL;

    $event_index = $this->fetch($portal_calendar, $year, $month, $day);
    if ($day) {
      $event_builder = $this->entityTypeManager()->getViewBuilder('portal_event');
      $event_index = $event_builder->viewMultiple($event_index);
      $day = new \DateTime("$year-$month-$day");
      $build = array(
        '#theme' => 'calendar_day',
        '#events' => $event_index,
        '#day_title' => t('Events at @date', ['@date' => $day->format('d M')]),
      );
      $event_index = \Drupal::service('renderer')->renderRoot($build);
    }
    return new JsonResponse($event_index);
  }

  public function html_get(PortalCalendar $portal_calendar, Request $request) {
    $get = $request->query;
    if (!$get->has('year') or !$get->has('month')) {
      return ['#plain_text' => 'none'];
    }
    
    $year = $get->get('year');
    $month =  $get->get('month');
    $day = $get->has('day') ? $get->get('day') : NULL;

    $event_index = $this->fetch($portal_calendar, $year, $month, $day);

    if ($day) {
      $event_builder = $this->entityTypeManager()->getViewBuilder('portal_event');
      $event_index = $event_builder->viewMultiple($event_index);
      return $event_index;
    }
    else {
      foreach ($event_index as $day => $events) {
        foreach ($events as $event) {
          $table[] = array(
            'day' => ['#plain_text' => $day],
            'name' => ['#plain_text' => $event['name']],
            'tag' => ['#plain_text' => $event['tag']],
          );
        }
      }
      $table = ['#type' => 'table'];
      $table['#header'] = ['Day', 'Name', 'Tag'];
      return $table;
    }
  }

  public function fetch($portal_calendar, $year, $month, $day = NULL) {
    if ($day) {
      $start_text = "$day-$month-$year";
      $period_text = 'P1D';
      $event_entry = 'monthly_event_entry';
    }
    else {
      $start_text = "01-$month-$year";
      $period_text = 'P1M';
      $event_entry = 'daily_event_entry';
    }

    $start_date = new \DateTime($start_text);
    $end_date = clone($start_date);
    $end_date->add(new \DateInterval($period_text));

    $event_storage = $this->entityTypeManager()->getStorage('portal_event');
    $query = $event_storage->getQuery();
    $query->condition('calendar_id', $portal_calendar->id());
    $query->condition('dates.value', $start_date->format('Y-m-d') , '>=');
    $query->condition('dates.end_value', $end_date->format('Y-m-d') , '<');
    $event_ids = $query->execute();

    $events = $event_storage->loadMultiple($event_ids);
    if ($day) {
      return $events;
    }

    $event_index = [];
    foreach ($events as $id => $event) {
      $date = new \DateTime($event->dates->value);
      $day = $date->format('j');
      if (!isset($event_index[$day])) {
        $event_index[$day] = [];
      }
      $event_entry = array(
        'name' => $event->name->value,
      );
      if (!$event->field_event_tags->isEmpty()) {
        $event_entry['tag'] = $event->field_event_tags[0]->entity->name->value;
      }
      $event_index[$day][] = $event_entry;
    }
    return $event_index;
  }
}
