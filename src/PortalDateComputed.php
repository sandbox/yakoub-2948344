<?php
namespace Drupal\portal_calendar;

use Drupal\datetime\DateTimeComputed;
use Drupal\Core\Datetime\DrupalDateTime;

class PortalDateComputed extends DateTimeComputed {

  public function getValue($langcode = NULL) {
    if ($this->date !== NULL) {
      return $this->date;
    }

    $item = $this->getParent();
    $value = $item->{($this->definition->getSetting('date source'))};

    $storage_format = 'Y-m-d H:i:s';

    if ($value) {
      $date = DrupalDateTime::createFromFormat($storage_format, $value, DATETIME_STORAGE_TIMEZONE);
    }
    else {
      $date = DrupalDateTime::createFromDateTime(new \DateTime());
    }

    if ($date instanceof DrupalDateTime && !$date->hasErrors()) {
      $this->date = $date;
    }

    return $this->date;
  }
}
