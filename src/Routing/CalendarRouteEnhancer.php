<?php

namespace Drupal\portal_calendar\Routing;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Routing\EnhancerInterface;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Enhances Field UI routes by adding proper information about the bundle name.
 */
class CalendarRouteEnhancer implements EnhancerInterface {

  /**
   * {@inheritdoc}
   */
  public function enhance(array $defaults, Request $request) {
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  protected function applies(Route $route) {
    if (!$route->hasOption('_field_ui')) {
      return false;
    }
    $options = $route->getOptions();
    kint($options);
    \Drupal::logger('enhancer')->info(print_r($options, true));
    return false;
  }
}
