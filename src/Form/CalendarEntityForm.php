<?php
namespace Drupal\portal_calendar\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class CalendarEntityForm extends BundleEntityFormBase {
  protected $calendar_storage;

  public function __construct(EntityStorageInterface $storage) {
    $this->calendar_storage = $storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('portal_calendar')
    );
  }

  public function form(array $form, FormStateInterface $form_state) {
    $calendar = $this->entity;
    if ($calendar->isNew()) {
      $form['#title'] = $this->t('Add calendar');
    }
    else {
      $form['#title'] = $this->t('Edit calendar');
    }

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $calendar->label(),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['calendar_id'] = [
      '#type' => 'machine_name',
      '#default_value' => $calendar->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['name'],
      ],
    ];
    
    $form = parent::form($form, $form_state);
    return $this->protectBundleIdElement($form);
  }

  public function exists($calendar_id) {
    $calendar = $this->calendar_storage->load($calendar_id);
    return !empty($calendar);
  }
}
