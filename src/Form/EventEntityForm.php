<?php

namespace Drupal\portal_calendar\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

class EventEntityForm extends ContentEntityForm {
    
  public function save(array $form, FormStateInterface $form_state) {
    $saved = parent::save($form, $form_state);
    $params = ['portal_event' => $this->entity->id()];
    $form_state->setRedirect('entity.portal_event.canonical', $params);
    return $saved;
  }
}
