var portal_calendar_day_events = null;

(function($, $ds) {
  var calendar = {};

  if (portal_calendar_day_events == null) {
    portal_calendar_day_events = function(events, calendar_item) {
      var events_element = $(events)
      var day = calendar_item.get(0).dataset.dayIter;
      events_element.dialog({title: 'Events as ' + day});
    };
  }

  Drupal.behaviors.portal_calendar_block = {
    attach: function(context) {
      $(context)
        .find('.block-portal-calendar')
        .once('portal-calendar_block')
        .each(function() {
          calendar.init($(this));
        });
    }
  };

  calendar.init = function(portal_calendar) {
    this.today = new Date();
    this.month = this.today.getMonth() + 1;
    this.year = this.today.getFullYear();

    var self = this;
    portal_calendar.find('ul.nav li.next').on('click', function() {
      self.next_month();
    });
    portal_calendar.find('ul.nav li.prev').on('click', function() {
      self.prev_month();
    });
    portal_calendar.on('click', function(_event) {
      var target = $(_event.target);
      var calendar_item = target.parentsUntil('ul.week-day', 'li[title]');
      if (calendar_item.length) {
        self.day_events(calendar_item);
      }
    });

    this.month_events();
  }

  calendar.next_month = function() {
    if (this.month == 12) {
      this.month = 1;
      this.year++;
    }
    else {
      this.month++;
    }
    this.month_events();
  };

  calendar.prev_month = function() {
    if (this.month == 1) {
      this.month = 12;
      this.year--;
    }
    else {
      this.month--;
    }
    this.month_events();
  };

  calendar.month_events = function() {
    var url = $ds.path.baseUrl + $ds.path.pathPrefix;
    url += 'calendar/organization/fetch';
    var data = {
      month: this.month,
      year: this.year
    };
    var self = this;
    $.post(url, data, function(response){
      self.events = response;
      self.draw_month();
    });
  }

  calendar.day_events = function(calendar_item) {
    var url = $ds.path.baseUrl + $ds.path.pathPrefix;
    url += 'calendar/organization/fetch';
    var day = calendar_item.get(0).dataset.dayIter;
    var data = {
      month: this.month,
      year: this.year,
      day: day
    };
    var self = this;
    $.post(url, data, function(response){
       portal_calendar_day_events(response, calendar_item);
    });
  }

  calendar.draw_month = function() {
    var week_tpl = document.getElementById('calendar-week').innerHTML;
    var view = {weeks: []};
    var first_day = new Date(this.year, this.month - 1, 1);
    var skip = first_day.getDay();
    var last = new Date(this.year, this.month, 0).getDate();

    this.month_array(skip, last, view);
    document.getElementById('portal-calendar-block').innerHTML = Mustache.render(week_tpl, view);
    var current_month = this.months[this.month - 1];
    $('ul.nav li.current').text(current_month + ' ' + this.year);
  };

  calendar.month_array = function(skip, last, view) {
    var week = [];
    var week_count = skip;
    var day_iter = 1;
    while(skip--) {
      week.push('');
    }
    var entry = null;

    while(day_iter <= last) {
      entry = {day: day_iter, name: null};
      if (this.events.hasOwnProperty(day_iter)) {
        entry.name = this.events[day_iter][0].name;
        entry.tag = this.events[day_iter][0].tag;
        entry.day_iter = day_iter;
      }
      week.push(entry);
      day_iter++;
      week_count++;
      if (week_count > 6) {
        week_count = 0;
        view.weeks.push({days: week});
        week = [];
      }
    }
    if (week_count > 0) {
      while (week_count < 7) {
        week.push('');
        week_count++;
      }
      view.weeks.push({days: week});
    }
  };

  calendar.months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

})(jQuery, drupalSettings);
